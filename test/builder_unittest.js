let chai = require('chai')
let should = chai.should()
let moment = require('moment')

let OutputBuilder = require('../cls/output_build')

describe('Output Builder', () => {
    describe('output builder', () => {
        it('should output in correct order', () => {
            let builder = new OutputBuilder(' ');
            builder.build('firstName')
                .build('lastName')
                .build('gender')
                .build('birthdate', (x) => { return moment(x).format('M/D/YYYY') })
                .build('color')

            let testObj = {
                firstName: 'Tester',
                lastName: 'Test',
                gender: 'Male',
                birthdate: new Date(1975,1,1),
                color: 'Red'
            }

            let res = builder.output(testObj)
            res.should.eql('Tester Test Male 2/1/1975 Red')

        })
    })
})
