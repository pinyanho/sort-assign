let chai = require('chai')
let should = chai.should()
let expect = chai.expect()
let moment = require('moment')

let SpaceTransform = require('../cls/transforms').SpaceTransform;
let CommaTransform = require('../cls/transforms').CommaTransform;
let PipeTransform = require('../cls/transforms').PipeTransform;

describe('Transformations', () => {
    describe('space transform', () => {
        it('should transform a correct form into persons correctly', () => {
            let spaceTransform = new SpaceTransform();
            let result = spaceTransform.transform('Kournikova Anna F F 6-3-1975 Red')
            result.should.have.property('firstName').eql('Kournikova')
            result.should.have.property('lastName').eql('Anna')
            result.should.have.property('gender').eql('Female')
            result.should.have.property('color').eql('Red')
            result.should.have.property('birthdate').eql(new Date(1975,5,3))

        })
    })

    describe('comma transform', () => {
        it('should transform a correct form into persons correctly', () => {
            let commaTransform = new CommaTransform();
            let result = commaTransform.transform('Abercrombie, Neil, Male, Tan, 2/13/1943')
            result.should.have.property('firstName').eql('Abercrombie')
            result.should.have.property('lastName').eql('Neil')
            result.should.have.property('gender').eql('Male')
            result.should.have.property('color').eql('Tan')
            result.should.have.property('birthdate').eql(new Date(1943,1,13))
        })
    })

    describe('pipe transform', () => {
        it('should transform a correct form into persons correctly', () => {
            let pipeTransform = new PipeTransform();
            let result = pipeTransform.transform('Smith | Steve | D | M | Red | 3-3-1985')
            result.should.have.property('firstName').eql('Smith')
            result.should.have.property('lastName').eql('Steve')
            result.should.have.property('gender').eql('Male')
            result.should.have.property('color').eql('Red')
            result.should.have.property('birthdate').eql(new Date(1985,2,3))
        })
    })



});

