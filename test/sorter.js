/* global describe, it */
'use strict';

let assert = require('assert');
let exec = require('child_process').exec;

let path = require('path');
let fs = require('fs')

describe('sorter bin', function () {
    let cmd = 'node ' + path.join(__dirname, '../bin/sorter') + ' ';
    let sPath = path.join(__dirname, '../files/space')
    let pPath = path.join(__dirname, '../files/pipe')
    let cPath = path.join(__dirname, '../files/comma')
    let expectedOutput = fs.readFileSync('./test/output_expected', 'utf8')
    console.log(cmd);

    it('should output correctly', function (done) {
        exec(cmd + '-s ' + sPath + ' -p ' + pPath + ' -c ' + cPath, function (error, stdout, stderr) {
            assert(!stderr)
            assert.equal(stdout, expectedOutput);
            done();
        });
    });

});
