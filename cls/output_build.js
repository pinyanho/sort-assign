class OutputBuilder {
    constructor(delim) {
        this.delim = delim
        this.order = []
    }

    build(props, transform) {
        if (transform == null) {
            transform = (x) => x;
        }

        this.order.push({ prop: props, transform: transform })

        return this;
    }

    output(objectInput) {
        let strOutput = [];
        this.order.forEach((item) => {
            if (!objectInput.hasOwnProperty(item.prop)) {
                throw ReferenceError('Invalid Object Property: ' + item);
            }

            strOutput.push(item.transform(objectInput[item.prop]))

        })

        return strOutput.join(this.delim)
    }


}


module.exports = OutputBuilder;
