

class Adapter {
    constructor(items) {

        this.items = items


    }

    addItem(item) {
        this.items.push(item)

    }


    get size() {
        return items.length;
    }



}

class SortAdapter extends Adapter {
    constructor(items, outputFunc) {
        super(items);
        if (outputFunc == null) {
            this.outputFunc = (x) => x;

        } else {
            this.outputFunc = outputFunc;
        }



    }

    sort(criterions, asc) {

        let prop = criterions[0]
        let func = (a,b) => {  if (a[prop] > b[prop]) return 1; else if (a[prop] < b[prop]) return -1; else return 0}
        let ret = firstBy(func, asc ? 1 : -1)
        for (let i = 1; i < criterions.length; i++) {

            ret = ret.thenBy(criterions[i], asc ? 1 : -1)
        }
        this.items.sort(ret);

    }


    outputItem(pos) {
        // Stub for implementation
        return this.outputFunc.output(this.items[pos])

    }

    get getAll() {
        let ret = []
        this.items.forEach((item) => {
            ret.push(this.outputFunc.output(item))
        })
        return ret;
    }


}

module.exports = {
    Adapter: Adapter,
    SortAdapter: SortAdapter
};
