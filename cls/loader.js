let fs = require('fs')
let readline = require('readline')

class Loader {
    load(filePath, transformation) {
        let res = fs.readFileSync(filePath).toString().split('\n');
        let listItems = []
        res.forEach((item)=> {
            listItems.push(transformation.transform(item))
        })

        return listItems;
    }
}

module.exports = Loader;
