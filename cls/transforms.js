let moment = require('moment');
let Person = require('./person');

class DataTransform {
    constructor(delim) {
        this.delim = delim;
        this.keywords = {}
    }

    registerKeyword(keyword, idx, transform) {
        if (transform == null) {
            transform = (x) => x;
        }
        this.keywords[keyword] = {idx: idx, transform: transform}
    }

    transform(strInput) {
        let tokens = strInput.split(this.delim).map( (item) => { return item.trim(); })
        let person = new Person();

        Object.keys(this.keywords).forEach((k) => {
            let obj = this.keywords[k]
            let ret = obj.transform(tokens[obj.idx])

            person[k] = ret;
        })

        return person;

    }



}

class SpaceTransform extends DataTransform {
    constructor() {
        super(' ');
        this.registerKeyword('firstName', 0)
        this.registerKeyword('lastName', 1)
        this.registerKeyword('gender', 3, maleFemale);
        this.registerKeyword('birthdate', 4, (x) => { let y = moment(x, 'M-D-YYYY'); return y.toDate() });
        this.registerKeyword('color', 5)
    }
}

class CommaTransform extends DataTransform {
    constructor() {
        super(',');
        this.registerKeyword('firstName', 0)
        this.registerKeyword('lastName', 1)
        this.registerKeyword('gender', 2, (x) =>  { if (x == 'Male' || x == 'Female') return x; else throw new TypeError('Invalid Gender') })
        this.registerKeyword('birthdate', 4, (x) => { let y = moment(x, 'M/D/YYYY'); return y.toDate() });
        this.registerKeyword('color', 3)

    }
}

class PipeTransform extends DataTransform {
    constructor() {
        super('|');
        this.registerKeyword('firstName', 0)
        this.registerKeyword('lastName', 1)
        this.registerKeyword('gender', 3, maleFemale )
        this.registerKeyword('birthdate', 5, (x) => { let y = moment(x, 'M-D-YYYY'); return y.toDate() });
        this.registerKeyword('color', 4)

    }
}

let maleFemale = (x) => {
    if (x == 'M') return 'Male';
    else if (x == 'F') return 'Female';
    else throw new TypeError('Invalid Gender');
};

module.exports = {
    DataTransform: DataTransform,
    SpaceTransform: SpaceTransform,
    CommaTransform: CommaTransform,
    PipeTransform: PipeTransform
}

