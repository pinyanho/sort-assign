# sorting-assignment

**Exercise for assignment by a company**

## Description

A commander CLI app

## Prerequisites

You must install
- Node.js (7.4.0)

## Usage

To install sorter from npm, run:

```
$ npm install sorter
```

## Run

```node ./bin/sorter -s <space file> -p <pipe file> -c <comma file>```

Where

```
-s --space <path to space file>
-p --pipe <path to pipe file>
-c --comma <path to comma file>
```


## Acknowledgments

Built using [generator-commader](https://github.com/Hypercubed/generator-commander).
